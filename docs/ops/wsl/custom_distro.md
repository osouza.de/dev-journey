---
id: custom_distro
title: Custom distro
slug: custom_distro
tags:
    - linux
    - wsl
---

# WSL Custom distro

You can use distros from the MS store or you can import one if it is not available there. More info can be found at the reference url.

0. Create a directory to the WSL stuff
```shell
mkdir C:\Linux

cd C:\Linux

mkdir C:\Linux\ArchLinux
```


## Exporting

I have'nt used the "Docker inside WSL to get a tar file" approach. Instead, I've done it through the powershell itself.

I decided to generate my Arch Linux tar file from a Docker container.

1. Create the docker container
```shell
docker run -it --rm --name arch archlinux sh
```

2. Export the container to a tarball
```shell
docker export $(docker ps -aqf name=arch) > ./arch.tar
```

2.1 Note, the manual asks you to use docker in order to leverage `grep` and `awk`. But you can have them in windows as well:
```
command | wsl -e awk '{print $1}'
```

You can `exit` the archlinux container or kill it with `docker stop`. It will be deleted automatically.


## Importing

1. It is pretty much easy to import the distro
```shell
wsl --import Arch C:\Linux\Arch .\arch.tar
```

2. Then, make it the default distro for wsl if you want
```
wsl -s Arch
```


## Setting up arch linux

Refer to [arch setup manual](../linux/arch.md).

## Settings

You can have more info in order to setup at "Microsoft Learn - WSL: Advanced settings configuration in WSL" (bottom link).


  - /etc/wsl.conf
```
[user]
default = d4nd
```

  - %UserProfile%\.wslconfig

## References
  - [Microsoft Learn - WSL: use custom distro](https://learn.microsoft.com/en-us/windows/wsl/use-custom-distro)
  - [Microsoft Learn - WSL: Advanced settings configuration in WSL](https://learn.microsoft.com/en-us/windows/wsl/wsl-config)