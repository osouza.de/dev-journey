# Utils

## Error 0x80040326
A lot of users referenced this error code and the solution was to update WSL via PowerShell:
```
wsl --update
```

## az login error

When trying to login with Azure CLI, the followin error may happen:

```shell
az login

The command failed with an unexpected error. Here is the traceback:
0. The ID token is not yet valid. Make sure your computer's time and time zone are both correct. Current epoch = 1676156217.  The id_token was:
# [...]
```

It is an [open WSL bug (microsoft/WSL#8204)](https://github.com/microsoft/WSL/issues/8204) where WSL2 date is set incorrectly after waking from sleep which seems to be causing this alot at the moment.


Solution:

```shell
sudo hwclock -s
sudo ntpdate time.windows.com
```

Note: on arch linux wsl you may be required to install `ntpdate `

```shell
yay -S ntpdate 
```


## libcrypt error on Docker
If you face this problem while trying to run a docker-compose setup:

```
docker-compose -f docker-compose.simple.yml

[1497] Error loading Python lib '/tmp/_MEIOOdEeS/libpython3.7m.so.1.0': dlopen: libcrypt.so.1: cannot open shared object file: No such file or directory
```

That can be solved by installing the lib:
```
pacman -S libxcrypt-compat
```

## Elevate privileges on PowerShell
```powershell
start-process powershell –verb runAs
```


## References

  - [serverfault: Gaining administrator privileges in PowerShell](https://serverfault.com/questions/11879/gaining-administrator-privileges-in-powershell)
