---
sidebar_position: 1
id: snippets
title: Snippets
tags:
    - docker
    - basic
    - snippets
---
# Docker Snippets

## MitM proxy!
```
docker run --rm -it -v $PWD/.mitmproxy:/home/mitmproxy/.mitmproxy -p 8080:8080 mitmproxy/mitmproxy
```

## Alpine

This'll create a alpine linux container and install the basic tools
```
docker run -it --rm -v "${PWD}:/test:rw" --name alpine alpine sh

apk add curl git gpg ssh vim
```

## NodeJS
```
docker run -it --rm -v "${PWD}:/app:rw" -w /app -p 3000:3000 --user node --name node node:17 bash
```

## Python

```
docker run -it --rm -v "${PWD}:/app:rw" -w /app -p 5000:5000 --user python --name python python bash
```


## React


### Creating a new project
The following piece of code generates a basic React application

```
docker run -it --rm -v "${PWD}:/app:rw" -w /app --user node --name node node:14 bash

npx create-react-app bahlinktri
```

### Running a dev project



## Docusaurus

### Creating a new project

[Project URL](https://docusaurus.io/docs)

```
## Create the project directory and get into it
mkdir <project_name>

## Start a Docker container
docker run -it --rm -v "${PWD}:/app:rw" -w /app -p 3000:3000 --user node --name node node:17 bash

### Inside the container, create a Docusaurus App
npx -y create-docusaurus@latest <project_name> classic --package-manager npm


## Then, exit the container, cd into the project dir, run docker and start the App
cd <project_name>

docker run -it --rm -v "${PWD}:/app:rw" -w /app -p 3000:3000 --user node --name 

docusaurus node:17 bash

npx docusaurus start -h 0.0.0.0
```

https://docusaurus.io/docs/next/docs-introduction#docs-only-mode



https://react-icons.github.io/react-icons/icons?name=fa