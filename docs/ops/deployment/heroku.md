---
id: heroku
title: Heroku
tags:
    - dev
    - deployment
---

1 - Initialize Heroku on your machine

```
heroku login
heroku container:login
```

2 - Create a new project

```
heroku create <project-name>
```

**Important** If you are deploying a **docker** app, you should set the stach to container at the creation time or, if you already have created the app, set it later:

```
## Define container stack on creation
heroku create --stack=container

## Set the stack of a already existent app
heroku stack:set container
```

Create a "heroku.yml" file with the following content:
```
build:
  docker:
    web: Dockerfile
```

3 - If you need to set some env vars:

Use **single quotes**

```
heroku config:set API_URL='http://localhost:5000/'
```

## Deploying

When deploying your app to Heroku, you can choose one of the three methods below:


### Heroku Git

If you already have created a Heroku App...

Add the Heroku remote and push to it's master branch.

```
heroku git:remote -a <project-name>
git push heroku master
```

If you want to push another branch to heroku master...

```
git push heroku <local-branch>:master
```


### GitHub

This option allows you to deploy, automatically also, via GitHub. To do so, just follow the steps...


### Container Registry

```
heroku container:push web
heroku container:release web
heroku open
```


## Extras

### Stopping releases

1 - Get the running releases

```
heroku ps
=== release (Free): bundle exec rake db:migrate
release.5129: up 2016/02/01 12:17:45 (~ 2s ago)
```

2 - Then, provide the name of the one-off dyno to the heroku ps:stop command:

```
heroku ps:stop release.5129
```
