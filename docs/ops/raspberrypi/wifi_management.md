---
id: wifi_management
title: WiFi device management
slug: raspberrypi/wifi_management
tags:
    - hostapd
    - raspberrypi
---


Let's setup a WiFi network that can be used to connect to our Raspberry PI so we can manage it.


1. First of all, install `hostapd`

```
apt install hostapd -y
```

Raspberry's pi builtin wireless card allows virtual devices. You can check it with:

```
iw list

...
	Supported interface modes:
		 * IBSS
		 * managed
		 * AP
		 * monitor
		 * P2P-client
		 * P2P-GO
		 * P2P-device
...
	valid interface combinations:
		 * #{ managed } <= 1, #{ monitor } <= 1, #{ P2P-device } <= 1, #{ P2P-client, P2P-GO } <= 1,
		   total <= 4, #channels <= 2
		 * #{ managed } <= 1, #{ AP } <= 1, #{ monitor } <= 1, #{ P2P-client } <= 1, #{ P2P-device } <= 1,
		   total <= 5, #channels <= 1
...
```

2. Create the virtual interface and assign an IP address:

```
iw dev wlan0 interface add uap0 type __ap

ip addr add 192.168.191.1/24 dev uap0
```

Make the interface not be managed by Network Manager
```
nmcli dev status
DEVICE         TYPE      STATE                   CONNECTION
wlan0          wifi      connected               <somessid>
uap0           wifi      disconnected            --
p2p-dev-uap0   wifi-p2p  disconnected            --
p2p-dev-wlan0  wifi-p2p  disconnected            --
eth0           ethernet  unmanaged               --
lo             loopback  unmanaged               --

nmcli dev set uap0 managed no

nmcli dev status
DEVICE         TYPE      STATE                   CONNECTION
wlan0          wifi      connected               <somessid>
p2p-dev-wlan0  wifi-p2p  disconnected            --
eth0           ethernet  unmanaged               --
lo             loopback  unmanaged               --
uap0           wifi      unmanaged               --
p2p-dev-uap0   wifi-p2p  unmanaged               --
```

We'll set the above steps plus random MAC to be executed at every boot.
 
Create a udev rule at `/etc/udev/rules.d/90-wireless.rules`
```
ACTION=="add", SUBSYSTEM=="ieee80211", KERNEL=="phy0", \
    RUN+="/usr/sbin/iw phy %k interface add uap0 type __ap" \
    RUN+="/usr/bin/ip addr add 192.168.191.1/24 dev uap0" \
    RUN+="/usr/bin/macchanger -r uap0"
ACTION=="add", SUBSYSTEM=="net", KERNEL=="uap0", ENV{NM_UNMANAGED}="1"
```


3. Set `/etc/hostapd/hostapd.conf` file as follow:
```
interface=uap0
driver=nl80211
#. SSID with a empty space would be nice
ssid= 
channel=6
ignore_broadcast_ssid=1

wpa=2
wpa_passphrase=0123456789
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP

hw_mode=g
macaddr_acl=0
auth_algs=1
```

4. Enable `hostadp` service on boot and start it now:
```
systemctl enable --now hostapd
```


## Wrap up
The above setup should create a hidden WiFi network:
  - SSID: " " # Yes, just a literal empty space.
  - Password: 0123456789
  - Channel: 6
  - MAC: random
  - IP: 192.168.191.1
  - Subnet: 192.168.191.1/24


## REFs

  - [RPi fórum - Pi 3 as wiireless client and wireless AP?](https://forums.raspberrypi.com/viewtopic.php?t=138730)
  - [Hawksites - Configuring hostapd on Raspberry Pi](https://hawksites.newpaltz.edu/myerse/2018/06/08/hostapd-on-raspberry-pi/comment-page-1/)
  - [Permanently add wireless interfaces with iw](https://superuser.com/questions/759542/how-to-permanently-add-wireless-interfaces-with-iw)