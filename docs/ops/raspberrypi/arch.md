---
id: arch
title: Arch Linux
slug: raspberrypi/arch
tags:
    - arch
    - raspberrypi
---

TODO

1. Install the Alarmpi on microsd
2. Configure wifi
3. Create new user, remove alarmpi and change root password
# useradd -m -s /bin/zsh d4nd
# passwd d4nd
# gpasswd -a d4nd wheel
# userdel -r alarm
# passwd
##Uncomment #%wheel in /etc/sudoers


4. Set DNS
# nano /etc/systemd/resolved.conf
"
[Resolve]
DNS=1.1.1.1 8.8.8.8
FallbackDNS=9.9.9.10 8.8.4.4 2606:4700:4700::1111 2620:fe::10 2001:4860:4860::8888
#Domains=
#LLMNR=yes
#MulticastDNS=yes
DNSSEC=allow-downgrade
#DNSOverTLS=no
#Cache=yes
#DNSStubListener=yes
#ReadEtcHosts=yes
"

06 - Update!
# pacman -Syu

00 - Install aurman
$ git clone https://aur.archlinux.org/aurman.git
$ cd aurman
$ makepkg -Acs
## If we got GPG key erros...
$ gpg --recv-key %KEYID
# pacman -U aurman...pkg.tzr.xz

07 - Install some stuff
# pacman -S base-devel docker git zsh sudo wget openvpn neofetch screen
# aurman -S docker-compose
# systemctl start docker
# systemctl enable docker
## The following docker image doesn't worked
# docker run -it --rm archlinux bash -c "echo hello world"
# docker run -it --rm arm64v8/alpine sh -c "echo Hello world"

08 - Changing hostname!

09 - Config SSH only privkeys

- Neofetch on bash-rs login

scp .zshrc alarmpi:.zshrc
scp -r .zsh alarmpi:.zsh
scp -r .zplug alarmpi:.zplug
scp -r .oh-my-zsh alarmpi:.oh-my-zsh

## References

  - [Customizing an Arch ARM image using QEMU ](https://lexruee.ch/customizing-an-arch-arm-image-using-qemu.html)