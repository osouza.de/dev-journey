---
id: snippets
title: Snippets
slug: raspberrypi/snippets
tags:
    - snippets
    - raspberrypi
---


## Listing available WiFi networks
```
nmcli dev wifi list
```

## Listing known WiFi networks
```
nmcli con | grep wifi
```

## Connecting to WiFi with password
```
nmcli dev wifi connect "<network_ssid>" password "<network_password>"
```

## Connecting to a known network
```
nmcli con up "<network_name>"

# Or with the BSSID
nmcli d wifi c "00:11:22:33:44:55"
```
