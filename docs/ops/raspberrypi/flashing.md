---
sidebar_position: 1
id: flashing
title: Flashing images
slug: raspberrypi/flashing
tags:
    - raspberrypi
---

**PLEASE be careful when using `dd`!** If you miss, you can **DESTROY** your entire drive.

1. Discover the device ID that you want to flash the image file to.
```
fdisk -l

Disk /dev/sda: 465,76 GiB, 500107862016 bytes, 976773168 sectors
Disk model: Samsung SSD 850 
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: AC304A8D-3EF0-474C-A4F9-FD3F376DC29F

Device         Start       End   Sectors   Size Type
/dev/sda1       4096    618495    614400   300M EFI System
/dev/sda2     618496 958313125 957694630 456,7G Linux filesystem
/dev/sda3  958313126 976768064  18454939   8,8G Linux filesystem


Disk /dev/sdb: 59,48 GiB, 63864569856 bytes, 124735488 sectors
Disk model: STORAGE DEVICE  
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x90ad0df1

Device     Boot    Start       End   Sectors  Size Id Type
/dev/sdb1           8192    137215    129024   63M  e W95 FAT16 (LBA)
/dev/sdb2         137216 124735487 124598272 59,4G  5 Extended
/dev/sdb5         139264    204797     65534   32M 83 Linux
/dev/sdb6         204800    729085    524286  256M  c W95 FAT32 (LBA)
/dev/sdb7         729088  63324157  62595070 29,8G 83 Linux
/dev/sdb8       63324160  63610877    286718  140M  c W95 FAT32 (LBA)
/dev/sdb9       63610880 124735487  61124608 29,1G 83 Linux
```

Usually, 'sda' is your hard drive (as my example shows)!

Do not choose it. In the example, `/dev/sdb` is my SD card. You can see that it has 9 partitions. Ignore that and use only `/dev/sdb` as the device identifier because we want to flash into the whole drive instead of a specific partition.


In the following examples, `/dev/sXY` should be replaced accordingly.

## Regular .img file
```
dd if=image_file.img of=/dev/sXY bs=4M status=progress conv=sync,noerror
```

## Compressed .img.xz file
If the image you have is XZ compressed, you can do this way:

```
xzcat kali-linux-2022.2-raspberry-pi-armhf.img.xz | dd of=/dev/sXY bs=4M status=progress conv=sync,noerror
```