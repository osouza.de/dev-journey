---
sidebar_position: 3
id: blackarch
title: BlackArch
tags:
    - infosec
    - linux
---
# BlackArch

If you plan to do some CTFs or anything security-related... You'll need some specific tools

## Strap
You can strap the [BlackArch](https://blackarch.org/) repository into your Arch / Manjaro system, [guide](https://blackarch.org/downloads.html#install-repo):

```
# Download the script
curl -O https://blackarch.org/strap.sh

# Verify the sha1-sum!
echo 8bfe5a569ba7d3b055077a4e5ceada94119cccef strap.sh | sha1sum -c

# Set execute bit
chmod +x strap.sh

# Run strap.sh
sudo ./strap.sh

sudo pacman -Syu
```

You can check the BlackArch tools with `sudo pacman -Sg | grep blackarch`

## Install some tools

```
yay -S android-apktool android-sdk-build-tools binwalk dalfox dirbuster evil-winrm genymotion ghex ghidra gobuster hashcat hashid jadx jadx-gui-desktop john mitmproxy net-snmp nmap nikto nuclei proxychains rustscan seclists sqlmap smbmap uniscan whatweb wfuzz wireshark wordlistctl wpscan zaproxy xxd 7-zip

sudo pacman -S goreleaser hydra webshells

pip install dex2jar flask-unsign frida-tools objection Responder
```

