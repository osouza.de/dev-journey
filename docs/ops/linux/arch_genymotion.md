---
sidebar_position: 2
id: genymotion
title: Genymotion on Arch linux
tags:
    - genymotion
    - linux
---


## Listing VMs
```
vboxmanage list vms

"Google Pixel 3" {cb8a0062-ff7b-4549-a85d-7c19870b8816}
"Google Nexus 6" {2b7745ab-122c-44d3-bdae-612a048dc06a}
```


## Starting VM from cli
With the device ID obtained from previous command, start the desired AVD
```
genymotion-player -n cb8a0062-ff7b-4549-a85d-7c19870b8816
```


## Genymotion Shell
If you need to run change some AVD setting, or even reset it, you can use `genymotion-shell`

```
genymotion-shell

Connection mode: local host
Welcome to Genymotion Shell

Genymotion virtual device selected: Google Pixel 3

Genymotion Shell > help

List of available commands:
---------------------------
android             information related to the Android system included in Genymotion distribution
battery             actions and information related to the battery widget
devices             generic actions related to virtual devices (listing, selection, ...)
diskio              actions and information related to the disk throughput
exit                quit this application
genymotion          information related to the Genymotion system
gps                 actions and information related to the Global Positioning System widget
help                display this help and display help for each verb
network             actions and information related to the network quality of service
pause               perform a pause (useful for automatic tests)
phone               actions related to the phone widget
quit                quit this application
rotation            actions related to the rotation of virtual devices
version             version number of the running Genymotion Shell application
```


## Fixing "Virtualbox host-only network interface error"

If you face this issue, might be a host kernel modules missing so Genymotion and virtualbox cannot properly start VMs.

If can fix it by installing your system's proper packages.

1. Check your linux kernel:
```
uname -a

Linux idpad 5.15.38-1-MANJARO #1 SMP PREEMPT Mon May 9 07:52:21 UTC 2022 x86_64 GNU/Linux
```

2. See what is the correct package to your machine
```
pacman -Ss virtualbox-host-modules | grep 515

extra/linux515-virtualbox-host-modules 6.1.34-4 (linux515-extramodules)
community/linux515-rt-virtualbox-host-modules 6.1.34-2 (linux515-rt-extramodules)
```

Mine's was `Linux 5.15.38` so I did `grep 515`. **Be sure to adapt accordingly to your case.**

3. Install it
```
sudo pacman -S linux515-virtualbox-host-modules

resolving dependencies...
looking for conflicting packages...

Packages (1) linux515-virtualbox-host-modules-6.1.34-4

Total Installed Size:  0,24 MiB
Net Upgrade Size:      0,00 MiB

:: Proceed with installation? [Y/n]
```

4. Reboot your machine and you'll good to go.