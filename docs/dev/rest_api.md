---
id: rest_api
title: REST API
tags:
    - dev
---
# REST API

**Standards**

Here'll define some standards to be used across the API endpoints.


## Listing resources

A API resource list endpoint should provide: [sorting](#sorting), [filtering](#filtering) and, most important, [pagination](#pagination).

**Return:**
```json
{
    "start": 0, /*offset value: (page - 1) * limit*/
    "count": 25, /*(limit value)*/
    "totalCount": 1000, /*(total resources count)*/
    "totalPages": 40, /*(totalCount - count)*/
    "data":[
        /*array with the objects*/
    ]
}
```

### Sorting

We should be able to sort the results by the object parameters.

**Usage**
- **Parameter**: `sort_by`
- **Attribute**: `field_name:direction(,other_field:direction)?`

If you need to sort more than one field, simply put a ',' and set the order plus the field itself.

If no order were defined after the attribute, 'ascending' will be used as the default direction: `sort_by=name` is the same as `sort_by=name:asc`

**Examples:**
- To sort users by 'name' in ascending order: `/user?sort_by=name`
- To sort newest articles (descending 'date'): `/article?sort_by=date:desc`
- To sort newest articles and 'title' alphabetically: `/article?sort_by=date:desc,title:asc`

If multiple fields were set, handle as a 'AND' operator.


### Filtering

Provide a way to search:

- Exact matches, ie name='John'
- Partial matches, ie names starting with 'J'
- Numeric values: equal (eq), different (ne), greater (gt), lesser (lt)...
- Negative operators for exclusion filtering.
- Also multiple field filtering.

**Usage**
- **Parameter**: `filter_by[{field_name}]`
- **Attribute**: `(eq, ne, lt, lte, gt, gte, like):{value}`

If no attribute operator was set, 'Equal' will be used as default.

If a field was set twice, handle as a 'AND' operation.

<details>
    <summary>Attributes explained</summary>

| Name | Value |
|---|---|
| Equal | eq |
| Not equal | ne |
| Less than | lt |
| Less and equal than | lte |
| Greater than | gt |
| Greater or equal than | gte |
| Like | like |
</details>

**Examples**:
- To filter users by 'first name': `/users?filter_by[first_name]=John`
- To filter articles with 'Test title' title and '2021-12-03' date: `/article?filter_by[title]=Test+title&filter_by[date]=2021-12-03`
- To filter users which name starts with 'J' (partial filtering): `/user?filter_by[first_name]=like:J`
- Articles published in May: `/article?filter_by[date]=gte:2022-05-01&filter_by[date]=lte:2022-05-31`
- Products with price less than 380: `/product?filter_by[price]=lt:380`

### Pagination

We'll use **offset pagination**.

<details>
    <summary>Usage:</summary>

| **Parameter** | **Attribute** | **Explanation** |
|---|---|---|
| limit | int()| Page size. ie: 25 items |
| offset | int() | Amount to skip. ((page - 1) * limit). ie: 2nd page of 25 items: ((2 - 1 * 25) = 25 |
</details>

**Examples:**
- Retrieve first page with 20 users: `/users?limit=20&offset=0`
- Retrieve third page with 20 users: `/users?limit=20&offset=40`


### Wrapping everything up:

This way, the API will be able to provide several usefull functions to resource listing process, as we can see in the following example:

- Retrieve 20 users from the first page of the newest users which first name starts with 'A': `/users?filter_by[first_name]=like:A&order_by=created_at:desc&limit=20&offset=0`

- Retrieve 20 users from the third page of the first created users called John: `/users?filter_by[first_name]=John&order_by=created_at&limit=20&offset=40`

- Retrieve 20 users from the third page of the users called John: `/users?filter_by[first_name]=John&order_by=created_at&limit=20&offset=40`

## TODO:
 - filter and sort by nested elements (?)
 - noSQL and SQL (?)

## Refs:
 - [Moesif Blog - REST API Design: Filtering, Sorting, and Pagination ](https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/)
 - [Tania Rascia - REST API: Sorting, Filtering, and Pagination](https://www.taniarascia.com/rest-api-sorting-filtering-pagination/)
  - [Restful API - Status codes](https://restfulapi.net/http-status-codes/)