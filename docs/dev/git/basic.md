---
sidebar_position: 1
id: git_basic
slug: basic
title: Basic
tags:
    - basic
    - git
---

# Git - Basic

Git daily use manual


## Default editor
```
git config --global core.editor vim
```


## Encoding

**Please**, if your team has Windows developers AND/OR you want to avoid LF / CRLF problems ([more info](https://stackoverflow.com/questions/1967370/git-replacing-lf-with-crlf))...

```
git config --global core.autocrlf false
```


## Modes

If you enconter several files changed with:
```
old mode 100644
new mode 100755
```

Run the following git config to set it GLOBALLY.

```
git config --global core.filemode false
```

## Authentication

First you'll need to authenticate yourself, as most of the "corporative repositories" are private.

A nice approach to do so is to use SSH Keys.

Config your git to identify yourself:
```
git config --global user.email "<your git email>"
git config --global user.name "<your name>"
```

### SSHKey

1. Follow the instructions [here](../../sec/encryption/ssh/key) to get your SSHKey.

2. Add your pubkey to your git provider account

    - [GitHub](https://github.com/settings/ssh/new)
    - [GitLab](https://gitlab.com/-/profile/keys)


### GPGKey

1. Follow the instructions [here](../../sec/encryption/gpg/key) to get your GPGKey.

2. Add your public GPGKey (result from $GPGKEY_PB) to your git provider

    - [GitHub](https://github.com/settings/gpg/new)
    - [GitLab](https://gitlab.com/-/profile/gpg_keys)

3. Set your environment to use your GPG Key

```
git config --global user.signingkey $GPGKEY_FP
git config --global commit.gpgsign true
git config --global tag.gpgsign true
git config --global gpg.program "gpg"
```


## Initializing a empty repo

1. Create a directory 

```
mkdir <repository_name>
```

2. Enter on it, initialize a git repository and set the remote url

```
cd <repository_name>
git init
git remote add origin git@gitlab.com:example/example-project.git
```

3. Create a file or start doing your code.

4. Add the modifications, you can check those by running `git status`

```
git add <file(s)/dir(s)>
```

5. Finally, commit the changes with a message explaining the changes.

```
git commit
```

6. Push the commit to the git remote repository

```
git push origin <branch_name>
```

## Getting a remote branch
If you do need to get a remote branch:
```
git fetch
git pull origin branch-name:branch-name
```
The remote branch will be available in your local machine.


## Comparing branches

For instance, you need to see what does the branch `dev` has that `master` doesn't:
```
git diff master...dev
```

;)


## Releases and Tags

When you need to deliver the code, you need to create a Tag and then Release it so the source-code will be provided as a zip or tar.gz file.

In the example, we'll release the version 1:
```
git tag v1.0.0
git push origin v1.0.0
```

Then, you need to go the the git provider web interface and create a Release:

- Go to the repository URL, Tag section
- Create release

// TODO improve


Now you'll have a zip file containing the repository source-code.

## Refs

[Git LF and CRLF](https://stackoverflow.com/questions/1967370/git-replacing-lf-with-crlf)
