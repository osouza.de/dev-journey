---
sidebar_position: 2
id: git_advanced
slug: advanced
title: Advanced
tags:
    - advanced
    - git
---

# Git - Advanced


## Patching
If you have an series of changes and, for instance, you want to apply it over your forked repo. It would be nice if you make use of **Patches**

1. List the changes you've made
```
git diff
```

2. Generate the patch file for the changes you want.
```
git diff >> project.patch

# Or if you want just for one file...
git diff file >> file.patch
```

3. Now, when you need to apply it, just copy into your local repository and apply it.
```
git apply project.patch
```

Done!


## Undoing local commit

If you commited something, didn't pushed yet and want to change it, you can do a reset:

```
git reset --soft HEAD~1
```

All the changes will be placed at the `staging area`.


## Clearing staging area
```
git restore -S .
```
or with

```
git reset HEAD -- .
```

**Note** the ``.`` means to consider everything. Instead, you can also use the dir / filename.


Then, the changes will stay as "Changes not staged for commit"

To remove the modifications, (it will erase the diffs on each file):
```
git checkout -- .
```

**Note** the ``.`` means to consider everything. Instead, you can also use the dir / filename.


## Cleaning local untracked files
If you have a lot of "untracked" files and to to remove by once, you can first check the files that'll be removed:

```
git clean -n
```

Then remove
```
git clean -f
```

More options can the found at the manual.


## Amending commit

If you messed up and a change didn't got into your commit and it was merged...

1. Checkout to the branch you want to fix, in this case "dev"

```
git checkout dev
```

2. Do your changes and add it with `git add <file>`

3. Do the git commit amend

```
git commit --amend --no-edit
```

4. Push!!! (and hope the GP didnt saw your mess haha)

```
git push origin -f dev
```


## Undoing commit

** CAUTION: this is dangerous**

If you were supposed to commit and push your work to a "fix_5" branch but you mistakenly pushed to "dev" branch...

1. First, create the correct branch with your modifications and send to the origin repo

```
    git checkout -b fix_5
    git push origin fix_5
```

2. Now, checkout to the dev branch and revert the changes to a previous commit

```
    git checkout dev
```

3. Check the commits with git log

```
git log -q

    commit e659ba99e100bda05d7172d5be0f946d99df9939 (HEAD -> dev, origin/dev)
    Author: Anderson de Souza <gmail@osouza.de>
    Date:   Thu Mar 24 19:37:59 2022 +0000

        Sending data from websites's Contato form to maya CRM

    commit 08780277bd89397d79ed1f707d443623e54c289a (origin/dev, dev)
    Author: Anderson de Souza <gmail@osouza.de>
    Date:   Thu Mar 24 16:26:53 2022 +0000

        Added 'conta' parameter to 'solicitacao' endpoint
```

4. a - In this case, we want to undo the e659ba commit and get back to the 087802

```
git revert 08780277bd89397d79ed1f707d443623e54c289a
```

The method above will leave history. You'll able to see a "revert" commit. If you want to avoid that:

4. b - Instead of doing revert, you can do git reset

```
git reset 08780277bd89397d79ed1f707d443623e54c289a
```

5. Now, push the fixed dev branch

```
git push -f origin dev
```


### Example

Lets say you have 3 commits:

    C
    B
    A

`git revert B`, will create a commit that undoes changes in B.

`git revert A`, will create a commit that undoes changes in A, but will not touch changes in B

Note that if changes in B are dependent on changes in A, the revert of A is not possible.

`git reset --soft A`, will change the commit history and repository; staging and working directory will still be at state of C.

`git reset --mixed A`, will change the commit history, repository, and staging; working directory will still be at state of C.

`git reset --hard A`, will change the commit history, repository, staging and working directory; you will go back to the state of A completely.



### Example recreate master branch

**Caution, this is dangerous...**

As seen on [this](https://candost.blog/how-to-reset-first-commit-in-git/)

```
git checkout --orphan master
```

Then, you may do whatever you want (delete the whole repo and start with just a README.md file...)

```
git add .
git commit -m "Initial commit"
git push origin master
```

## References

    - [SpecBee - Git Patch](https://www.specbee.com/blogs/how-create-and-apply-patch-git-diff-and-git-apply-commands-your-drupal-website)
    - [devconnected - How To Remove Files From Git Commit](https://devconnected.com/how-to-remove-files-from-git-commit/)