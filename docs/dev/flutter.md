# Flutter

First of all, install and configure it properly in your system:
  - [Archlinux](../ops/linux/arch.md#flutter)

## VSCode

Extensions to be installed:

  - adelphes.android-dev-ext
  - toroxx.vscode-avdmanager
  - Dart-Code.flutter
  - Dart-Code.dart-code

Be sure to update your `$PATH` with your Android SDK...

`PATH=~/Android/Sdk:$PATH`

Then install Android SDK stuff:

  - **Build-tools**: `sdkmanager build-tools 30`
  - **Platform-tools**: `sdkmanager platform 30`
  - **System image**: `sdkmanager "system-images;android-30;google_apis;x86"`
  - **System image**: `sdkmanager "system-images;android-30;google_apis;x86_64"`

1. Create a Android virtual device:



After installing or making sure the above are installed you can go ahead on creating avd.

    Create an avd - avdmanager create avd -n TestAVD -k "system-images;android-30;google_apis;x86" --device "Pixel_4"
    List created avds emulator -list -avds
    Start Emulator or AVD emulator @TestAVD


## New project


