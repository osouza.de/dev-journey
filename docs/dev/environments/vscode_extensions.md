
## Markdown Preview Enhanced
shd101wyy.markdown-preview-enhanced


## WSL
ms-vscode-remote.remote-wsl

1. Install "Remote WSL" extension
2. CTRL + SHIFT + P > "Remote Explorer: Focus on WSL Targets View"
3. Open it
4. Add the project folder from the WSL distro path instead of "open local"
5. Install the extensions you need again (A button "install in xxx workspace" will appear)