---
sidebar_position: 1
title: External Resources
tags:
    - infosec
    - resources
---

The following URL's provide a lot of usefull resources!


## GTFOBins
GTFOBins is a curated list of Unix binaries that can be used to bypass local security restrictions in misconfigured systems.
- https://gtfobins.github.io/


## HackTricks
Welcome to the page where you will find each hacking trick/technique/whatever I have learnt in CTFs, real life apps, and reading researches and news.
- https://book.hacktricks.xyz


### HackTricks - Boitatech
Portuguese: página onde você encontrará truques, dicas e técnicas de tudo do que aprendi em CTFs e aplicações reais, incluindo pesquisas, notícias e artigos.
- https://hacktricks.boitatech.com.br/


## OSINT Tools

- [LookyLoo](https://lookyloo.circl.lu/)

- [Cuckoo](https://cuckoo.ee)

- https://osintframework.com/

- [VirusTotal](http://virustotal.com/)

- [URL Scan](https://urlscan.io/)

## OWASP

### Common Attacks list
- https://owasp.org/www-community/attacks/

###  Top 10
- https://owasp.org/Top10/

## Tools

  - [WhatIsMyBrowser](https://developers.whatismybrowser.com/)

  - [UserAgents.io - UA parser](https://useragents.io/parse)
