---
title: Processes
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---

## Remote Code Execution
```
search "" and ""
// | where $table !in ("")
| where $table in ("IdentityDirectoryEvents", "DeviceEvents", "DeviceFileEvents", "DeviceNetworkEvents")
| where TimeGenerated between(( - 15s) .. ( + 15s))
| order by TimeGenerated asc
// To get the queries ran by the process:
//| where AdditionalFields['OperationDetails'] != ""
//| project AdditionalFields['OperationDetails']
```



## Fileless attack toolkit detected


1 - Check processes for the reported file
```
DeviceProcessEvents
| where FileName == "testinitsigs.exe"
| where DeviceName has ""
| order by TimeGenerated asc
```


2 - Check the filehash on VirusTotal



## Checking activities : WIP

Something like "<SubjectLogonId>0x15828334</SubjectLogonId>" will be given.


Search for the "SubjectLogonId"
```
search "0x15828334"
```


Checking every security event related to that login

```
SecurityEvent
| where TargetLogonId == "0x15828334"
| project-reorder SubjectAccount, TargetAccount, WorkstationName
```

### Logon id
"TargetLogonId" = HEX
"LogonId" = DEC

Converting the "TargetLogonId" from HEX to DEC

  - 0x15828334
  - 360874804



At least the following tables will be returned:

  - DeviceLogonEvents
  - DeviceProcessEvents
  - DeviceEvents
  - SecurityEvent

Of course we can add all the Device* tables...


```
DeviceLogonEvents
| project-reorder LogonId
```

LogonId = 360874804



We need this if we want to search only the DeviceEvents

```
DeviceEvents
| where InitiatingProcessLogonId == "360874804"
| order by TimeGenerated asc
| project TimeGenerated, ActionType, AdditionalFields, InitiatingProcessFolderPath
```