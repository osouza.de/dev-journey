---
title: Azure Active Directory
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---

# Azure Active Directory

## New Azure AD audit events detected

Search AuditLogs for the initiating account or activity name:

```kql
AuditLogs
//| where ActivityDisplayName in ("")
//| where InitiatedBy contains ""
| summarize arg_max(OperationName, ResultDescription, InitiatedBy, *) by TimeGenerated
| order by TimeGenerated asc
```

`ActivityDisplayName` can be something like "Create business flow", "Create request", "Auto apply review". Usually it comes from the alert "OperationName"
