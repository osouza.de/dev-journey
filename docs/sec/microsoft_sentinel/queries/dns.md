---
title: DNS
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---

# DNS

## TI map IP entity to DnsEvents

1. See the DNS query events

```kql
DnsEvents
| where IPAddresses has "69.16.175.42"
| order by TimeGenerated asc
| project TimeGenerated, Computer, ClientIP, Name, IPAddresses
```

Results:

09/02/2023 15:22:34.670
MACHINEDC.contoso.org
10.1.1.5
tg1.aniview.com
69.16.175.42, 69.16.175.10


So, the DNSs were:
  - player.aniview.com
  - tg1.aniview.com


2. Search for more activity relating to the domain and or IP

```kql
search 'aniview' and 'MACHINEDC'
| where $table !in ("")
| where TimeGenerated > ago(2d)
| order by TimeGenerated asc
```

Than we analyze the results, looking for what initiated the DNS query (executable, link, etc).


Further investigation can be done using the following queries:


3. What opened the URL

```kql
DeviceNetworkEvents
| where RemoteUrl has "player.aniview.com"
| order by TimeGenerated asc
| project TimeGenerated, ActionType, DeviceName, InitiatingProcessAccountUpn, InitiatingProcessCommandLine, RemoteIP
```


4. Check every procces ran at the machine

```kql
DeviceProcessEvents
| where DeviceName has "MACHINEDC"
| where TimeGenerated > ago(2d)
| order by TimeGenerated asc
| project TimeGenerated, FileName, FolderPath, ProcessCommandLine, MD5, InitiatingProcessFolderPath
```