---
title: Networking
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---

Connection to some IP

```
DeviceNetworkEvents
| where DeviceName has "hostname"
// | where RemoteIP == "target ip"
// | where LocalIP == ""
// | where TimeGenerated > ago(2d)
| where TimeGenerated between(datetime("2022-12-30 14:00:00") .. now())
| order by TimeGenerated asc
| summarize arg_max(RemoteIP, *) by TimeGenerated
```



MS Azure Security Insights
```kql
let GetIPStats = (Ip_Address: string, start: datetime, end: datetime) {
    //checking time span to lock to 7 days or less for Entity page usage
    let end_start = datetime_diff('day', end, start);
    let start_time = iff(end_start > 7, end - 7d, start);
    let end_time = end;
    let IpStats = (union isfuzzy=true
            (
            VMConnection
            | where TimeGenerated between (start_time .. end_time)
            | where SourceIp =~ Ip_Address
            | where SourceIp != DestinationIp
            | where Direction =~ "outbound"
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                OutboundCount = countif(Direction =~ "outbound")
                by
                IPAddress = SourceIp,
                Type,
                RemoteIPAddress = DestinationIp,
                Direction,
                SentBytes = tolong(BytesSent),
                ReceivedBytes = tolong(BytesReceived)
            ),
            (
            VMConnection
            | where TimeGenerated between (start_time .. end_time)
            | where DestinationIp =~ Ip_Address
            | where SourceIp != DestinationIp
            | where Direction =~ "inbound"
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                InboundCount = countif(Direction =~ "inbound")
                by
                IPAddress = DestinationIp,
                Type,
                RemoteIPAddress = SourceIp,
                Direction,
                SentBytes = tolong(BytesSent),
                ReceivedBytes = tolong(BytesReceived)
            ),
            (
            WireData
            | where TimeGenerated between (start_time .. end_time)
            | where LocalIP =~ Ip_Address
            | where LocalIP != RemoteIP
            | where Direction =~ "outbound"
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                OutboundCount = countif(Direction =~ "outbound")
                by IPAddress = LocalIP, Type, RemoteIPAddress = RemoteIP, Direction
            ),
            (
            WireData
            | where TimeGenerated between (start_time .. end_time)
            | where RemoteIP =~ Ip_Address
            | where LocalIP != RemoteIP
            | where Direction =~ "inbound"
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                InboundCount = countif(Direction =~ "inbound")
                by IPAddress = RemoteIP, Type, RemoteIPAddress = LocalIP, Direction
            ),
            (
            DeviceNetworkEvents
            | where TimeGenerated between (start_time .. end_time)
            | where LocalIP =~ Ip_Address
            | where LocalIP != RemoteIP
            | extend Direction = "outbound"
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                OutboundCount = countif(ActionType =~ "ConnectionSuccess")
                by IPAddress = LocalIP, Type, RemoteIPAddress = RemoteIP, Direction
            ),
            (
            DeviceNetworkEvents
            | where TimeGenerated between (start_time .. end_time)
            | where RemoteIP =~ Ip_Address
            | where LocalIP != RemoteIP
            | extend Direction = "inbound"
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                InboundCount = countif(ActionType =~ "InboundConnectionAccepted")
                by IPAddress = RemoteIP, Type, RemoteIPAddress = LocalIP, Direction
            ),
            (
            CommonSecurityLog
            | where TimeGenerated between (start_time .. end_time)
            | where SourceIP =~ Ip_Address
            | where SourceIP != DestinationIP
            //| where DeviceAction has_any ('allow', 'allowed', 'accept', 'built', 'start', 'connect', '')
            //| where not(DeviceAction has_any ('built','deny', 'denied', 'rst', 'blocked', 'teardown'))
            | extend Direction = iff(CommunicationDirection !in~ ('outbound', '0') or CommunicationDirection !in~ ('inbound', '1'), 'NotAvailable', CommunicationDirection)
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                InboundCount = countif(Direction in~ ('Inbound', '1')),
                OutboundCount = countif(Direction in~ ('Outbound', '0')),
                UnknownDirection = countif(Direction =~ 'NotAvailable')
                by
                IPAddress = SourceIP,
                Type = strcat(Type, ':', DeviceVendor, '-', DeviceProduct),
                RemoteIPAddress = DestinationIP,
                Direction,
                SentBytes = tolong(SentBytes),
                ReceivedBytes = tolong(ReceivedBytes)
            ),
            (
            CommonSecurityLog
            | where TimeGenerated between (start_time .. end_time)
            | where DestinationIP =~ Ip_Address
            | where SourceIP != DestinationIP
            //| where DeviceAction has_any ('allow', 'allowed', 'accept', 'built', 'start', 'connect', '')
            //| where not(DeviceAction has_any ('built','deny', 'denied', 'rst', 'blocked', 'teardown'))
            | extend Direction = iff(CommunicationDirection !in~ ('outbound', '0') or CommunicationDirection !in~ ('inbound', '1'), 'NotAvailable', CommunicationDirection)
            | summarize
                StartTime = min(TimeGenerated),
                EndTime = max(TimeGenerated),
                InboundCount = countif(Direction in~ ('Inbound', '1')),
                OutboundCount = countif(Direction in~ ('Outbound', '0')),
                UnknownDirection = countif(Direction =~ 'NotAvailable')
                by
                IPAddress = DestinationIP,
                Type = strcat(Type, ':', DeviceVendor, '-', DeviceProduct),
                RemoteIPAddress = SourceIP,
                Direction,
                SentBytes = tolong(SentBytes),
                ReceivedBytes = tolong(ReceivedBytes)
            )
        );
    IpStats
};
// Here we can set the IP
let start = datetime('2023-01-09T00:00:00.000Z');
let end = datetime('2023-01-10T00:00:00.000Z');
let ip = '';
GetIPStats(ip, start, end)
| summarize
    StartTime = min(StartTime),
    EndTime = max(EndTime),
    InboundTotal = sum(InboundCount),
    OutboundTotal = sum(OutboundCount),
    ReceivedBytesTotal = sum(ReceivedBytes),
    SentBytesTotal = sum(SentBytes),
    UnknownDirectionBytesTotal = sum(UnknownDirection)
    by IPAddress, RemoteIPAddress, Type
```