---
id: mitmproxy_genymotion
title: MitM Proxy and Genymotion
tags:
    - infosec
    - mitmproxy
    - android
---

# MitM Proxy and Genymotion

In order to Android accept MitM proxy certificate system-wide, we need to install it at the android device as follows.

**Note:** parts of this step-by-step were taken from the "MitM Proxy Docs" (URL at the bottom)


## Certificate file name

CA Certificates in Android are stored by the name of their hash, with a ‘0’ as extension (Example: c8750f0d.0). It is necessary to figure out the hash of your CA certificate and copy it to a file with this hash as filename. Otherwise Android will ignore the certificate. By default, the mitmproxy CA certificate is located in this file: `~/.mitmproxy/mitmproxy-ca-cert.cer`

1. Enter into mitmproxy directory (usually $HOME/.mitmproxy)
```
cd ~/.mitmproxy
```

2. Calculate the certificate hash and create a copy with it as the file name
```
hashed_name=`openssl x509 -inform PEM -subject_hash_old -in mitmproxy-ca-cert.cer | head -1` && cp mitmproxy-ca-cert.cer $hashed_name.0
```

## Installing into Android

We need to put the certificate in the /system/etc/security/cacerts/ at the Android filesystem. By default, the /system partition is mounted as read-only. 
So, if you are using Genymotion (which I recommend) do the following:


1. Ensure ADB is running with root mode
```
adb root
```

2. Remount the AVD filesystem
```
adb remount
```

3. Now you can send the certificate to the AVD: 

In this example, c8750f0d.0 is the cert file name. Be sure to use yours...
```
adb push c8750f0d.0 /system/etc/security/cacerts
```

4. Set the correct permission
```
adb shell chmod 644 /system/etc/security/cacerts/c8750f0d.0
```

5. Reboot the AVD
```
adb shell reboot
```

And that's it!


## Extras


### Android proxy
To set and reset android proxy settings, [look here](snippets.md#proxy-settings)


### Export mitmproxy to cURL

1. Inside mitmproxy, press ":" to enter into the command mode
2. Clear everything and type
```
export.clip curl @focus
```

**Recommended:** If you want to set a key bind, edit `~/.mitmproxy/keys.yaml`:
```
-
  key: ctrl e
  ctx: ["flowlist"]
  cmd: export.clip curl @focus
  help: Export the request to cURL, httpie etc.
```

I had a error when tried to get the shortcut working on Manjaro:
>   QXcbClipboard::setMimeData: Cannot set X11 selection owner


## Refs
  - [MitM Proxy - Docs](https://docs.mitmproxy.org/stable/howto-install-system-trusted-ca-android/)