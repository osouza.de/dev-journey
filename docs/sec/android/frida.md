---
id: frida
title: Frida
tags:
    - infosec
    - frida
    - android
---

## Setup

**Note:** You should have `frida-tools` pip package installed if you followed the [blackarch guide](../../ops/linux/blackarch.md)

Update your installation would be nice:
```
pip install --upgrade frida-tools
```


Download the `frida-server` accordingly your host's Frida version. So, discover what version it is.

1. Checking host's Frida version:
```
frida_version=$(frida --version) && echo $frida_version
```
In my case it was `15.1.22`

2. Download the correct version from the project repository.
```
wget "https://github.com/frida/frida/releases/download/${frida_version}/frida-server-${frida_version}-android-x86.xz"
```
Note the architecture! In my case, it was `x86`.

If the above wget command doesn't work, manually download it from the repository: https://github.com/frida/frida/releases/

3. Extract the Frida server
```
xz -d "frida-server-${frida_version}-android-x86.xz"
```

4. Send the file to the Android device
```
adb push "frida-server-${frida_version}-android-x86" /data/local/tmp/frida-server
```

5. Set execution permission
```
adb shell chmod 777 /data/local/tmp/frida-server -R
```

Almost good to go.


## Testing

1. Start Frida server at Android device
```
adb shell /data/local/tmp/frida-server &
```


## References
  - [FRIDA docs](https://frida.re/docs/home/)
  - [FRIDA docs - Android](https://frida.re/docs/android/)