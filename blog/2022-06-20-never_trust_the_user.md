---
date: 2022-06-20T10:00
slug: never-trust-the-user
title: Never trust the user
authors: [osouza.de]
tags: [hacking, history, webhacking]
---

Hello, friend.

Some guy was looking for bus tickets to go visit his parents. His city bus station had a website to buy tickets online.

His head started scratching, you know... So he decided to see how the website worked: when you learn web development, you are supposed to learn the "Browser's Developer Tools" (Chrome/Chromium, Firefox, etc., they all have them). In that tool, you are able to mess with a lot of stuff: JS console, cookies, HTML body, HTTP requests, and so on.


## Vulnerability Assessment
He started analyzing every request... chose his ticket and went through the purchase.

During the checkout, he noticed that the tickets' info was sent back to the server in the request! What does it mean??? "Well," he thought, "as I'm sending the price back, can I set it?"


## Exploitation
So, instead of "R$57,60" for the ticket price, he changed the request and set it at "R$57,69."

Crossed his fingers and... That worked! The balance has been debited from the account and the order receipt was generated.

But it was not done yet. You must print the receipt and exchange it for the ticket itself at the bus station.


## Problems
What if the guys at the bus station notice the price difference? May he face a fraud accusation? Let's make a superficial Law analysis.

### Brazil's Criminal Law - Art 154a
   - [Original (portuguese)](http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del2848.htm#art154a)
   - [English translation](https://sherloc.unodc.org/cld//legislation/bra/codigo_penal/title_i/article_154a-b/art._154a-b.html?lng=en&tmpl=sherloc)

>**Art. 154-A.** Trespassing another party´s computer-related device, whether connected or not to the World Wide Web, by means of undue violation of a security mechanism, to obtain, tamper with, or destroying data or information without express or tacit authorization by the device owner, or to install vulnerabilities in order to obtain an illicit advantage:
>
>Penalty - imprisonment, from 3 (three) months to 1 (one year), and fine.

Well, we can see clearly that no advantage was obtained in this hacking. Actually, he spent nine cents on that.

But, as I am not a lawyer, I cannot guarantee.

As he didn't know what to do with that info, he just ignored it... His reward was always bypassing and breaking stuff, not making money.


## A few years later
He remembered the issue and went after it to see how it was.

The website was the same (actually, it still is). But the vulnerability has been mitigated.

They didn't refactor the solution. The user still sends the ticket's info to the server, but... Even if you add one cent, the server checks it and returns with an error message saying that the ticket price was changed by the user.


## Never trust the user
With this history, we can see (in a real-life practical approach), what is said in almost every "Cybersecurity course": **Never trust the user!** While drawing, architecting, and coding software, always have this deeply engraved in your mind.

It doesn't matter if you are coding a Raspberry Pi program to monitor your bedroom temperature, an intranet message board website for the company where you work, or an API in your work...


That's it!

## References
  - [JusBrasil - Hackers, Crackers and Criminal Law (Portuguese)](https://grmadv.jusbrasil.com.br/artigos/407334629/hackers-crackers-e-o-direito-penal)
  - [Sherloc - Brazil's Criminal Law English translated](https://sherloc.unodc.org/cld/v3/sherloc/legdb/search.html?lng=en&tmpl=sherloc#?c=%7B%22filters%22:%5B%7B%22fieldName%22:%22en%23legislation@country_label_s%22,%22value%22:%22Brazil%22%7D,%7B%22fieldName%22:%22legislation.nationalLawArticle@title_s%22,%22value%22:%22C%5Cu00F3digo%20Penal%22%7D%5D,%22match%22:%22%22,%22sortings%22:%22%22%7D)
