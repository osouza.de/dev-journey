---
sidebar_position: 1
title: Introdução
slug: /
---

# Dev Journey

O objetivo deste projeto é documentar a jornada no mundo do desenvolvimento e servir como um guia para os iniciantes e demais que buscam por referências.

Toda contribuição é bem vinda!

## URLs

Os links abaixo podem ser utilizados para acessar o presente website e seu repositório:

  - [devjourney.osouza.de](http://devjourney.osouza.de/)
  - [url do repositório](https://gitlab.com/osouza.de/dev-journey/)

## PS: Isso é um eterno Work In Progress ;)
