---
id: img_mount
title: Montando arquivos de imagem
slug: linux/img_mount
tags:
    - linux
    - misc
---

Veremos como montar um arquivo `.img` para acessá-lo a partir da nossa máquina host.


1. Pegue o layout de partição da imagem:

```
fdisk -lu retropie01-05-2020.img

Disco retropie01-05-2020.img: 7,41 GiB, 7948206080 bytes, 15523840 setores
Unidades: setor de 1 * 512 = 512 bytes
Tamanho de setor (lógico/físico): 512 bytes / 512 bytes
Tamanho E/S (mínimo/ótimo): 512 bytes / 512 bytes
Tipo de rótulo do disco: dos
Identificador do disco: 0xd42bc226

Dispositivo             Inicializar Início      Fim  Setores Tamanho Id Tipo
retropie01-05-2020.img1 *             8192   124927   116736     57M  e FAT16 W95 (LBA)
retropie01-05-2020.img2             124928 15523839 15398912    7,4G 83 Linux
```


3. Calcule o offset da partição que você quer montar vezes o tamanho do bloco

Nesse caso, não queremos montar a partição `/boot`, então pule ela e monte apenas o sistema de arquivos raíz.

```
124928 * 512 = 63963136
```


3. Montem a imagem para o loop usando o offset

```
losetup -o 63963136 /dev/loop0 retropie01-05-2020.img
```


4. Monte com:

```
mount /dev/loop0 /mnt/rasp
```


5. Para desmontar, use o comando a seguir

```
umount /mnt/rasp
losetup -d /dev/loop0
```