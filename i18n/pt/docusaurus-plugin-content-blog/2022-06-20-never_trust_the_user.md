---
date: 2022-06-20T10:00
slug: never-trust-the-user
title: Nunca confie no usuário
authors: [osouza.de]
tags: [hacking, history, webhacking]
---

Olá, amigo.

Um cara estava querendo comprar passagens de ônibus para visitar seus pais. A cidade onde ele morava tinha um site de venda de passagens.

Sua cabeça começou a coçar, sabe né... Então ele decidiu ver como o site funcionava: quando você aprende desenvolvimento web, na teoria você deveria aprender as "Ferramentas de Desenvolvedor do Navegador" (Chrome/Chromium, Firefox, etc., todos eles tem). Nessa ferramenta, você pode mexer com um monte de coisas: Console JS, cookies, corpo HTML, requisições HTTP e daí por diante.


## Avaliação de Vulnerabilidade
Ele começou a analisar cada requisição... escolheu a passagem e seguiu no processo.

Durante o checkout, ele percebeu que as informações da passagem era enviada de volta na requisição! O que isso quer dizer??? "Bem," ele pensou, "já que eu to enviando o preço de volta, será que eu posso escolher ele então?"


## Exploração
Então, ao invés de "R$57,60" pela passagem, ele trocou a requisição e definiu "R$57,69."

Cruzou os dedos e... Funcionou! O saldo foi debitado da sua conta e o recibo foi gerado.

Mas ainda não tinha acabado. Você precisa imprimir o recibo e trocar pelo bilhete lá na rodoviária.


## Problemas
Mas e se os funcionários da rodoviária notarem a diferença de preço? Será que ele enfrentaria um processo por fraude? Vamos fazer uma análise superficial da Lei.

### Código Penal Brasileiro - Art 154a
   - [Texto original](http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del2848.htm#art154a)

>**Art. 154-A.** Invadir dispositivo informático de uso alheio, conectado ou não à rede de computadores, com o fim de obter, adulterar ou destruir dados ou informações sem autorização expressa ou tácita do usuário do dispositivo ou de instalar vulnerabilidades para obter vantagem ilícita:
>
>Pena – reclusão, de 1 (um) a 4 (quatro) anos, e multa.

Bom, podemos ver claramente que nenhuma vantagem foi obtida nesse caso. Na verdade, ele até gastou nove centavos.

Mas, como eu não sou um advogado, não posso garantir.

Como ele não sabia o que fazer com essas informações, ele só ignorou... Sua recompensa sempre era burlar e quebrar as coisas e não fazer dinheiro.


## Alguns anos depois
Ele se lembrou do problema e foi fuçar nisso de novo.

O site estava igual (na verdade, ainda está). Mas a vulnerabilidade foi mitigada.

Eles não refatoraram o sistema. O usuário ainda envia as informações da passagem para o servidor, mas... Mesmo se tu colocar um centavo a mais, o servidor verifica e retorna uma mensagem de erro dizendo que o usuário mudou o preço da passagem.


## Nunca confie no usuário
Com essa história, pudemos ver (numa abordagem prática e da vida real), o que é dito na maioria dos "Cursos de cibersegurança": **Nunca confie no usuário!** Quando estiver desenhando, arquitetando e programando um software, sempre tenha essa premissa profundamente gravada na sua mente.

Não importa se você está programando um sisteminha para monitorar a temperatura do teu quarto numa Raspberry Pi, um quadro de mensagens na intranet da empresa onde você trabalha ou uma API no seu trabalho...


É isso aí!


## References
  - [JusBrasil - Hackers, Crackers e Código Penal](https://grmadv.jusbrasil.com.br/artigos/407334629/hackers-crackers-e-o-direito-penal)

