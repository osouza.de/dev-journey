---
date: 2021-05-30T10:00
slug: hello-world
title: Olá mundo
authors: [osouza.de]
tags: [vida, história]
---

Olá, amigo.

Vou tentar te dar um vislumbre da história que me trouxe pro mundo da T.I.

## Brincadeira de criança

Quando eu era criança, costumava desmontar meus brinquedos eletronicos pra tirar os motores e engrenagens. Pra que? Pra entender como funcionava e fazer funcionar de outras formas.

Fiquei perplexo quando consegui entender melhor como um motor escovado de corrente contínua funcionava. (falando nisso, tu pode aprender isso [aqui](https://stringfixer.com/pt/Shunt_wound_motor))

<details><summary>Motor DC</summary>
<p>

![motor DC](./dc_motor.jpg)
![compomentes motor DC](./dc_motor_components.jpg)

</p>
</details>

O problema é que eu quase nunca conseguia fazer aqueles brinquedos funcionar direito novamente :joy:


## Palmtop

Meu pai era vendedor (bebidas, café, farinha, ...) e as empresas para as quais ele trabalhava forneciam um dispositivo portátil (geralmente um PalmTop). E foi bem aí que a minha curiosidade começou:

Ele geralmente trazia o aparelho pra casa e, meus irmãos e eu, sempre pedíamos para jogar com ele. O pai deixava mas com uma condição: "não fucem demais! (queria dizer, não quebrem)"

<details><summary>PalmTop</summary>
<p>

![PalmTop](./palmtop.jpg)

</p>
</details>

Costumávamos desenhar e jogar paciência. Mas... eu aprendi que havia alguns arquivos "estranhos" nele, coisas do sistema. Eram aparelhos rodando Windows!
Levou um tempo pra eu descobrir que aquilo era um computador, mas não importa; minha curiosidade havia sido bem alimentada.

**P.S.** a gente nunca quebrou nenhum dos aparelhos do nosso pai.


## Desktop

Até 2007, eu tive muito pouco contato com computadores de uso doméstico (desktops, pra ser mais exato). Naquela época os computadores eram muito caros e a minha família tinha o suficiente para o nosso bem estar. Os computadores também não eram tão necessários para o uso doméstico comum.

Um amigo meu me ajudou a criar meu primeiro endereço de e-mail (o qual eu tenho até hoje). Era um Hotmail. Eu era uma viagem... Eu tinha uma máquina de escrever com a qual eu fiz tipo de um cartão de visitas com meu email e distribuí pra todos os meus amigos.

Daí eu criei minha página no Orkut e comecei a usar o computador como uma pessoa "comum".


### Script Kiddie

É aqui que o "hacking / engenharia social" começou a pipocar...

Comecei a fazer amizades no Orkut e eu tentava chamar eles no MSN. Mas alguns deles não tinham um MSN com o mesmo e-mail do Orkut. Aí eu percebi que o Orkut não verificava se a pessoa de fato possuia o e-mail cadastrado.

Uma vez eu precisei recuperar minha conta do Orkut e, pra isso, enviaram um URL no meu e-mail.

Juntei todas as coisas e pensei: "E se eu criar um e-mail pra cada um desses que não existem e pedir pra redefinir a senha no Orkut?"

*et voilá!*

Mas eu percebi que isso era muito demorado, pegar o e-mail da pessoa no orkut e ficar verificando se ele existia. Então eu achei uma página que fazia isso por mim.

Uma carrada de contas do Orkut foram perdidas nisso aí (F). Me arrependo profundamente.


### Programando

Meus pais conseguiram comprar um bom computador. Mas a gente ainda não tinha conexão com a internet. Meu irmão mais velho ia pra Casa de Cultura e usava os computadores e internet de lá pra baixar umas coisas e então trazia pra casa no pendrive dele.

Então eu tive melhor contato com a internet na 7ª série da escola. Um amigo e colega de sala tinha uma lanhouse e eu sempre fazia os trabalhos em grupo com ele. Geralmente eu fazia todo o trabalho pois ele me deixava ficar de graça nos PCs pelo resto do dia!

Eu imprimia um monte de apostilas de código. Eu ia online, baixava alguns PDFs e levava pra imprimir na gráfica.


#### Servidor Open Tibia

Ali eu também conheci o Tíbia, aprendi a jogar com eles e tal. Uma vez eu vi eles configurando o próprio servidor de Tíbia (projeto OpenTibia). Eu fiquei de cara e pedi pra eles compartilhar comigo o software.

Coloquei os arquivos no meu MP3 player (sim, era o único "pendrive" que eu tinha) e configurei meu próprio servidor em casa, offline.

Então as coisas evoluíram: consegui softwares mais sofisticados com website e banco de dados. Por conta disso eu aprendi [LUA](https://www.lua.org/), [PHP](https://www.php.net/), e [MySQL](https://www.mysql.com).

Fiz alguns amigos online e também conheci um dos melhores servidores de Tíbia da época (infelizmente eu não lembro o nome). Eu tive muita sorte de encontrar um **SHELL PHP** no site do servidor. Com isso eu toquei o terror. Criei uma conta de GM pra mim (no Tíbia se chama GOD) e devastei o servidor. Spawnei um monte de *Morgaroths* na frente do DP de Thais e muuuito mais...

Esperei o dono do servidor logar e o assustei. Tirei os acessos dele e coisas do tipo... Depois de uma conversa, eu meio que fui convidado a participar da equipe. Lá eu aprendi muito mais.

A bagunça no servidor foi tão absurda que um reset não foi suficiente. Eles tiveram que trocar o nome do servidor!


#### Conquer Online

Meu irmão mais velho tinha um instalador do Conquer Online. Meu outro irmão e eu costumávamos ficar olhando a tela de login, imaginando como seria o tal jogo. Até que um dia **a gente finalmente colocou internet** e começamos a jogar!

Não demorou muito tempo pra eu começar o meu próprio servidor de Conquer Online. Eu também fiz bastante amigos da minha cidade com esse server.


### Trabalho no Cybercafe

Consegui meu primeiro emprego numa lanhouse muito legal. Lá eu aprendi a cuidar dos computadores. Também tive mais tempo pra aprender e meio que recebi por isso.


### Consertos

Meu irmão mais velho e eu aprendemos a como arrumar nosso próprio computador em casa pois era caro pagar uma assistência técnica pra isso, e a gente geralmente conseguia bugar tudo. Além disso, nós queríamos que o computador funcionasse do nosso jeito.

Eu lembro a primeira vez que nós tentamos reinstalar o Windows no nosso PC... Apagou tudo que tinha porquê era um daqueles CDs que o fabricante usa pra tirar da fábrica e não um CD comum de instalação do Windows. A gente estava tomando sopa com nossos pais na mesa perto do computador quando a instalação terminou. Olhamos um pro outro, apavorados e saímos voando pro PC pra ver que **todos os nossos dados** (incluindo fotos da família) tinham sido completamente apagados. :grimacing:

A experiência veio e eu comecei a oferecer serviçoes de conserto na minha escola além de vizinhos e alguns conhecidos. Muitos professores meus me pagavam pra consertar seus computadores e tal. Eu nunca hackeei nenhum deles. **Eu juro**. Meu pai **sempre** oferecia meus serviços pros colegas de trabalho dele e eu comecei a arrumar telefones também.


## Laptop

Minha mãe me deu um notebook de presente. SÓ PRA MIM! Foi uma baita duma mudança na minha vida, pois eu não precisava mais compartilhar e esperar a minha vez pra acessar a internet e fazer minhas coisas.

Com isso, eu tinha mais tempo e privacidade, então eu comecei alguns projetos e fui pra "DeepWeb" onde eu aprendi uma PORRADA de coisa legal.


## Ensino Médio

No Ensino Médio, nós tinhamos uma sala de informática muito boa. No começo eram máquinas com Windows.


### Lutando contra a censura

A direção da escola havia proibido o Facebook, mas geral estava nem aí. Até que eles começaram a bloquear o acesso ao site. Eu, como bom rebelde, não curti nem um pouquinho e comecei a investigar o que eles fizeram. Eu vi que não era um Proxy, então seria mais fácil. Descobri que alguém tinha configurado o arquivo `hosts` no Windows tipo:
```
arquivo C:\Windows\System32\Drivers\etc\hosts

127.0.0.1 facebook.com
```
Eu comecei a pregar a palavra! Ensinei todo mundo como burlar a censura e navegar livrevemente na internet de novo!

Mas algo estranho começou a acontecer: alguns dos alunos que usaram o laboratório da escola pra acessar o Facebook, começaram a perder suas contas.

Então eu investiguei o que estava rolando. TADA! Encontrei **keyloggers** em cada um dos computadores do laboratório. Aqui eu devo confessar que invadi algumas das contas de pessoas que eu não gostava. Mas os keyloggers não eram meus, só obtive vantagem. Depois disso, eu desinstalei todos os keyloggers.

Então a direção da escola desistiu de tentar censurar a internet :sunglasses:


### Sendo um b4b4c4

Um programa do governo distribuiu computadores com Linux para a nossa escola. Que massa! O problema é que cada máquina vinha com SSH habilitado por padrão. Não demorou muito pra eu conseguir root.

Quando a minha turma ia pro laboratório de informática, eu logava em cada máquina via SSH e zoava com geral sem o conhecimento (e permissão). Dentro de cada PC, eu matava alguns processos tipo o navegador de internet ou editor de texto, ou pior ainda, fazia alguns arquivos (horas de digitação) simplesmente desaparecer. Nunca fui pego.

Tem um caso que aconteceu que não tenho certeza se posso divulgar aqui. Talvez no futuro...


### Sendo um cagueta

Naqueles dias tinha um site chamado "AskFM". Era uma página onde tu recebia umas perguntas dos teus seguidores / visitantes e as respondia publicamente.

Um cara teve a brilhante ideia de iniciar uma página anônima de difamação. As pessoas iam lá e perguntavam sobre alguns alunos e as respostas geralmente eram ácidas, tóxicas e difamatórias.

As vítimas não ficaram bem com isso e começaram uma caça às bruxas. Um cara chegou até mim e me ofereceu 50 pila se eu conseguisse pegar o anônimo. Eu aceitei a oferta, não pelo dinheiro, mas pelo desafio em si.

Não vou divulgar como eu fiz, mas cheguei no cara e o confrontei (ele era quase um amigo), e ele, sabendo que alguem tinha pago pra eu investigar, confessou pra mim e quase implorou pra eu manter em segredo. Não fiz.

O final da história é que o cara quase foi linchado por toda a escola, ele teve que chamar a polícia pra conseguir sair e foi zoado por todo mundo. Eu também quase apanhei do amigo dele que disse fazer parte da zoeira. E... até hoje to esperando meus cinquenta pila.


### Grupo ShareIT

Ao passo que eu aprendia a filosofia hacker, via que seria bom se eu pudesse:
  - aprender a programar
  - compartilhar meu conhecimento

Eu tive uma ideia muito eficiente pra conseguir ambos e então decidi iniciar um **grupo de estudos** na minha escola pois: você aprende quando ensina e aprender implica em receber (ganhar).

Mas infelizmente a ideia nunca evoluiu :sweat:

<details><summary>ShareIT grupo no Facebook</summary>
<p>

![ShareIT Facebook group](./facebook_shareit.jpg)

</p>
</details>


## além disso...

O resto da minha história eu vou publicar nos próximos dias. Não perca!
