---
sidebar_position: 1
title: Einführung
slug: /
---

# Dev Journey

Der Zweck dieses Repositorys ist, meine Reise in der 
Software-Entwicklungswelt zu 
dokumentieren und als eine Art Reiseführer für Newcomer und Interessierte zu dienen.

Jede Hilfe ist willkommen!

## URLs

Folgende URLs führen zur gegenwärtigen Website und zu ihrem Repository:

  - [devjourney.osouza.de](http://devjourney.osouza.de/)
  - [repository url](https://gitlab.com/osouza.de/dev-journey/)

## PS: Dieses Projekt wird ständig überarbeitet. ;)
