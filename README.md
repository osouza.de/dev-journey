# Dev Journey

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.


## Development

```
# Start the docker container
docker run -p 3000:3000 -v "${PWD}:/app:rw" --workdir /app --rm -it node:17 bash

# Inside the docker, start the docusaurus development server
npx docusaurus start -h 0.0.0.0
``` 